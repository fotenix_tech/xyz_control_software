#***********************************************************************************************************
#============================================== XYZ CONTROL ================================================
# PROJECT: FALLOW-FIELD XYZ SYSTEM
# AUTHOR: SHARIAR RAJIB, append as appropriate
# FILE: main_control.py
# LICENSE: N/A
# CREATED ON: 18/08/2020 14:00
# DISCLAIMER: The code written is for a pre-progammed XYZ sequence with fixed velocity, acceleration, 
# deceleration, etc. if any of these parameters are changed as of (30/08/2020), the author(s) 
# does not take any responsibility for equipment damage, loss of property, injury, or death. 
# Do not change the XYZ parameters unless absolutely certain of the associated behaviour.
#================================================== XYZ ====================================================
#***********************************************************************************************************

import os
import sys
import time
import json
import serial
import logging
import schedule
import configparser
import logging.config
import random, string
from time import sleep
from pathlib import Path
from threading import Event
from datetime import datetime, timedelta


#===================================== Logger & Config functions here ======================================
''' Initialise the logger library.
 The Logger is initialised with the config variable stored in the
 logging.conf file. Change the parameters in this file
 to alter the logging behaviour.'''
def LoggerInit():
    global logger
    try:
        currentPath = Path(os.getcwd())
        log_conf_path = currentPath / 'logging.conf'
        logging.config.fileConfig(log_conf_path)
        # create logger
        logger = logging.getLogger('main')
    except Exception as e:
        print('Logger setup error : ' + str(e))


''' Read the Experiment parameter from the experiment_config file. Edit this file
before running a new experiment so that the capture file naming and
the associated metadeta is accurate and ready for the analysis software.'''
def ReadConfigFile():
    global config
    logger.debug('Reading experiment config file.')
    try:
        currentPath = Path(os.getcwd())
        exp_conf_path = currentPath / 'experiment_config.ini'
        config = configparser.ConfigParser()
        config.read(exp_conf_path)
    except Exception as e:
        logger.error('Main config file read error: ' + str(e))
    

#======================================== Scheduling Functions here ========================================
def RunEnable():
    global stop_for_today
    stop_for_today = False
    # Check the config file for delay between consecutive captures
    delay_cap = config.getint('SCHEDULING', 'DelayBetweenCaptures')
    schedule.every(delay_cap).seconds.do(RunGRID)
    #schedule.every(delay_cap).minutes.do(RunGRID)
    #schedule.every(delay_cap).hours.do(RunGRID)
    logger.info("Program is activated for today...")

def RunDisable():
    global stop_for_today
    stop_for_today = True
    logger.info("Program is deactivated till tomorrow...")

def RunGRID():
    logger.info("Starting Grid...")
    global rec_number
    if stop_for_today:
        return schedule.CancelJob
    else:
        SerOpen()
        if CheckDriveMonitorStatus('X') == 'ok':
            if CheckDriveMonitorStatus('Y') == 'ok':
                # This is not a new experiment instead a recurring one
                # So setting new_exp to False and new_rec to True in CompileJSONData
                CompileJSONData(False, True, 1, 1, ' ', 1, ' ', ' ')
                StartXPresetSequence()
                SaveJSONData(db_json_dict)
                #RunHomeSequence()
                rec_number += 1
            else:
                logger.debug("Y-axis drive returned fault exit program.")
                force_exit_handler()
        else:
            logger.debug("X-axis drive returned fault exit program.")
            force_exit_handler()
        SerClose()


#==================================== XYZ Drive Control Functions here =====================================
'''Use Copley Control's [ASCII Programmer’s Guide] (see Docs folder in this repo) to understand 
the various ASCII commands used in this program. The following functions are used monitor various
status registers to log the process that the Drive is performing currently.
'''
# This function provides provides drive status information (Register: 0xa0).
def CheckDriveMonitorStatus(axis):
    drive_stat = 'ok'
    drive_ack = ''
    if ser.is_open:
        try:
            if axis == 'X':
                ser.write(('0 g r0xa0\r').encode())
            elif axis == 'Y':
                ser.write(('1 g r0xa0\r').encode())
            drive_ack = ser.readline()
        except Exception as e:
            logger.error("Serial Error: " + str(e))
            drive_stat = 'fault'
        drive_ack = drive_ack.rstrip().decode("utf-8")
        logger.debug("Monitor status cmd, Drive ACK: " + str(drive_ack))
        drive_monitor_status_bit = drive_ack.split()
        if drive_monitor_status_bit[0] == 'e':
            logger.debug("Drive returned error, checking code.")
            CheckErrorCodes(drive_monitor_status_bit[1])
            drive_stat = 'fault'
        else:
            if (int(drive_monitor_status_bit[1]) & (1<<9)):
                logger.debug(axis + "-axis " + "Positive limit switch active.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<10)):
                logger.debug(axis + "-axis " + "Negative limit switch active.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<11)):
                logger.debug(axis + "-axis " + "Drive enable input not active.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<12)):
                logger.debug(axis + "-axis " + "Drive is disabled by software.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<16)):
                logger.debug(axis + "-axis " + "Positive software limit condition.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<17)):
                logger.debug(axis + "-axis " + "Negative software limit condition.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<20)):
                logger.debug(axis + "-axis " + "Drive has been reset.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<22)):
                logger.debug(axis + "-axis " + "Drive fault. Check docs for more info.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<26)):
                logger.debug(axis + "-axis " + "Home switch is active.")
                drive_stat = 'fault'
            if (int(drive_monitor_status_bit[1]) & (1<<27)):
                logger.debug(axis + "-axis " + "Move in progress")
                drive_stat = 'fault'
    else:
        logger.info("Serial Port is not opened")
        drive_stat = 'fault'
    return drive_stat

# This function provides trajectory generator status information (Register: 0xc9). 
def CheckDriveTrajectoryStatus(axis):
    drive_ack = ''
    if ser.is_open:
        try:
            if axis == 'X':
                ser.write(('0 g r0xc9\r').encode())
            elif axis == 'Y':
                ser.write(('1 g r0xc9\r').encode())
            drive_ack = ser.readline()
        except Exception as e:
            logger.error("Serial Error: " + str(e))
        drive_ack = drive_ack.rstrip().decode("utf-8")
        logger.debug("Move complete cmd, Drive ACK: " + str(drive_ack))
        drive_traj_status_bit = drive_ack.split()
        if drive_traj_status_bit[0] == 'e':
            logger.error("Drive returned error, checking code.")
            CheckErrorCodes(drive_traj_status_bit[1])
        else:
            if (int(drive_traj_status_bit[1]) & (1<<11)):
                logger.error(axis + "-axis " + "Homing error.")
            if (int(drive_traj_status_bit[1]) & (1<<12)):
                logger.error(axis + "-axis " + "Homing referenced.")
            if (int(drive_traj_status_bit[1]) & (1<<13)):
                logger.error(axis + "-axis " + "Drive running home command.")
            if (int(drive_traj_status_bit[1]) & (1<<14)):
                logger.error(axis + "-axis " + "move aborted.")
            if (int(drive_traj_status_bit[1]) & (1<<15)):
                logger.error(axis + "-axis " + "running move command.")
    else:
        logger.info("Serial Port is not opened")


'''This function logs the meaning of the error code sent by the drive. 
Note: These function only checks the important error codes. For the full 
list of the available error codes refer to the ASCII Programmer’s Guide'''
def CheckErrorCodes(code):
    if code == '1':
        logger.debug("Too much data passed with command")
    elif code == '3':
        logger.debug("Unknown command code")
    elif code == '4':
        logger.debug("Not enough data was supplied with the command")
    elif code == '5':
        logger.debug("Too much data was supplied with the command")
    elif code == '9':
        logger.debug("Unknown parameter ID")
    elif code == '10':
        logger.debug("Data value out of range")
    elif code == '11':
        logger.debug("Attempt to modify read-only parameter")
    elif code == '16':
        logger.debug("Illegal serial port forwarding")
    elif code == '18':
        logger.debug("Illegal attempt to start a move while currently moving")
    elif code == '31':
        logger.debug("Invalid node ID for serial port forwarding")
    elif code == '32':
        logger.debug("CAN Network communications failure")
    elif code == '33':
        logger.debug("ASCII command parsing error")
    elif code == '36':
        logger.debug("Bad axis letter specified")
    else:
        logger.debug("Unknown error, check documentation for more info.")


'''
Use Copley Control's [Indexer 2 User Guide & CME User Guide] to program the XYZ system with preset sequence.
The Indexer 2 Program can store up to 32 sequences (0 to 31). Upon receiving a Go command,
the program executes the sequence that has been selected by a register (R0-R31).
This program assumes that Initiate GO fox X-axis is programmed as register (R0) and we need run sequence
number 4. As for Y-axis the Initiate GO is programmed as register (R4) and we need run sequence
number 4 (for Positive direction) and sequence number 5 (for Negative direction)
It also assumes that the X-axis has CAN ID (0) and Y-axis has CAN ID (1). Finally,
the program assumes that the after each move the Driver waits for a Register value of R1 (for x-axis),
R2 (for Y-axis positive sequence) and R3 (for Y-axis negative sequence) to increment. 
Value of this register starts from 1 to last Grid, for e.g. if the GRID is 5x5 the last value would be 5.
''' 
def StartXPresetSequence():
    logger.info("X Sequence starting.")
    drive_ack = ''
    global pos_number
    global plant_number
    pos_number = 0
    plant_number = 0
    x_pos_seq = config.get('XYZDRIVE', 'x_sequence_number')
    if ser.is_open:
        try:
            '''Write to CAN id 0 on register r0 value of 0x8005 (1000000000000101)
            Which is set bit 15 to initiate go and run sequence number 5 (lsb - 00101)'''
            ser.write(('0 i r0 0x800'+ x_pos_seq +'\r').encode())
            drive_ack = ser.readline()
            drive_ack = drive_ack.rstrip().decode("utf-8")
            logger.debug("Drive ACK: " + str(drive_ack))
        except Exception as e:
            logger.error("Serial Error: " + str(e))
        if drive_ack  == 'ok':
            logger.debug("Drive acked sequence go cmd.")
            # Run a loop for a GRID of (5x5)
            x = 0
            while x < 5:
                logger.debug("X-Axis loop counter: " + str(x))
                x += 1
                while True:
                    logger.debug("Waiting for move to complete.")
                    '''The controller monitors bit 27 of the event status
                    register to determine when the move is complete.
                    Set -> in motion'''
                    ser.write(('0 g r0xa0\r').encode())
                    drive_ack = ser.readline()
                    drive_ack = drive_ack.rstrip().decode("utf-8")
                    logger.debug("Move complete cmd, Drive ACK: " + str(drive_ack))
                    drive_ack_bit_27 = drive_ack.split()
                    if (int(drive_ack_bit_27[1]) & (1<<27)):
                        pass
                    else:
                        logger.debug("Move completed.")
                        '''The controller checks bit 14 of the trajectory status
                        register to determine if the move was aborted.
                        Set -> Aborted'''
                        ser.write('0 g r0xc9\r'.encode())
                        drive_ack = ser.readline()
                        drive_ack = drive_ack.rstrip().decode("utf-8")
                        logger.debug("Move abort cmd, Drive ACK: " + str(drive_ack))
                        drive_ack_bit_14 = drive_ack.split()
                        if (int(drive_ack_bit_14[1]) & (1<<14)):
                            logger.error("Move aborted, exit loop.")
                            # since the move was aborted for some reason, it's best
                            # to abort sequence by setting loop counter to out of range
                            x = 5
                            break
                        else:
                            '''The GRID is setup such that home is (0,0), 
                            thus to run a GRID of (5x5) first X-axis increments by 1 position
                            then the Y-axis increments in positive direction by 5 position. 
                            After which the X-axis increments by 1 position and then the Y-axis
                            increments in negative direction by 5 position. This continues until 
                            X-axis increments by the maximum possible value, in this case 5.'''
                            if x % 2 == 0:
                                StartYPresetSequence('NEG')
                            else:
                                # Although loop counter starts with x=0, x has already been 
                                # incremented by 1 as soon as program flow enters the loop 
                                StartYPresetSequence('POS')
                            ''' "Wait for Parameter" for X-axis is set on R1, meaning 
                            the drive will wait for the jetson to set this register 
                            with incrementing values to run the next step in the sequence. 
                            Currently there are 5 Move cmds for the X-axis,
                            thus the value of register should increase from 1 - 5''' 
                            ser.write(('0 i r1 ' + str(x) + '\r').encode())
                            drive_ack = ser.readline()
                            drive_ack = drive_ack.rstrip().decode("utf-8")
                            logger.debug("Wait for Parameter, Drive ACK: " + str(drive_ack))
                            break
        else:
            logger.error("Drive did not ack, check the drive is on and amp is hardware enabled")
    else:
        logger.info("Serial Port is not opened")
    logger.info("Grid complete.")

def StartYPresetSequence(dir):
    logger.debug("Y Sequence starting.")
    y_pos_seq = config.get('XYZDRIVE', 'y_positive_sequence_number')
    y_neg_seq = config.get('XYZDRIVE', 'y_negative_sequence_number')
    drive_ack = ''
    global pos_number
    try:
        '''Write to CAN id 1 on register r4 value of 0x8004 (1000000000000101) 
        implying, set bit 15 to initiate go 
        and run sequence number 4 (lsb - 00100) for positive direction 
        or 0x8005 for sequence number 5 for negative direction''' 
        if dir == 'POS':
            # Y axis rotate motor positive direction
            logger.debug("Y axis rotating motor positive direction.")
            ser.write(('1 i r4 0x800'+ y_pos_seq +'\r').encode())
        elif dir == 'NEG':
            # Y axis rotate motor negative direction
            logger.debug("Y axis rotating motor positive direction.")
            ser.write(('1 i r4 0x800'+ y_neg_seq +'\r').encode())
        drive_ack = ser.readline()
        drive_ack = drive_ack.rstrip().decode("utf-8")
        logger.debug("Drive ACK: " + str(drive_ack))
    except Exception as e:
        logger.error("Serial Error: " + str(e))
    if drive_ack  == 'ok':
        logger.debug("Y-axis drive acked sequence go cmd.")
        # Run a loop for a GRID of (5x5)
        y = 0
        while y < 5:
            logger.debug("Y-axis loop counter: " + str(y))
            pos_number += 1
            y += 1
            while True:
                logger.debug("Waiting for move to complete.")
                '''The controller monitors bit 27 of the event status
                register to determine when the move is complete.
                Set -> in motion'''
                ser.write(('1 g r0xa0\r').encode())
                drive_ack = ser.readline()
                drive_ack = drive_ack.rstrip().decode("utf-8")
                logger.debug("Move complete cmd, Drive ACK: " + str(drive_ack))
                drive_ack_bit_27 = drive_ack.split()
                if (int(drive_ack_bit_27[1]) & (1<<27)):
                    pass
                else:
                    logger.debug("Move completed.")
                    '''The controller checks bit 14 of the trajectory status
                    register to determine if the move was aborted.
                    Set -> Aborted'''
                    ser.write('1 g r0xc9\r'.encode())
                    drive_ack = ser.readline()
                    drive_ack = drive_ack.rstrip().decode("utf-8")
                    logger.debug("Move abort cmd, Drive ACK: " + str(drive_ack))
                    drive_ack_bit_14 = drive_ack.split()
                    if (int(drive_ack_bit_14[1]) & (1<<14)):
                        logger.error("Move aborted, exit loop.")
                        # since the move was aborted for some reason, it's best
                        # to abort sequence by setting loop counter to out of range
                        y = 5
                        break
                    else:
                        # Start image capture
                        StartImageAcquisition()
                        ''' "Wait for Parameter" for Y-axis is set on R2 (Sequence 4) or 
                        R3 (Sequence 5)register, meaning the drive will wait for 
                        the jetson to set this register with incrementing values to
                        run the next step in the sequence. Currently there are 5 Move cmds,
                        thus the value of register should increase from 1 - 5'''  
                        if dir == 'POS':
                            # Y axis rotate motor positive direction
                            ser.write(('1 i r2 ' + str(y) + '\r').encode())
                        elif dir == 'NEG':
                            # Y axis rotate motor negative direction
                            ser.write(('1 i r3 ' + str(y) + '\r').encode())
                        drive_ack = ser.readline()
                        drive_ack = drive_ack.rstrip().decode("utf-8")
                        logger.debug("Wait for Parameter, Drive ACK: " + str(drive_ack))
                        break
    else:
        logger.debug("Drive did not ack, check the drive is on and amp is hardware enabled")


def RunHomeSequence():
    logger.info("Starting homing...")
    x_home_seq = config.get('XYZDRIVE', 'x_home_sequence_number')
    y_home_seq = config.get('XYZDRIVE', 'y_home_sequence_number')
    drive_ack_x = ''
    drive_ack_y = ''
    if ser.is_open:
        try:
            '''Write to CAN id 0 on register r0 value of 0x8005 (1000000000000101)
            Which is set bit 15 to initiate go and run home sequence number'''
            # X-axis home seq command
            ser.write(('0 i r0 0x800'+ x_home_seq +'\r').encode())
            drive_ack_x = ser.readline()
            drive_ack_x = drive_ack_x.rstrip().decode("utf-8")
            logger.debug("X-axis Drive ACK: " + str(drive_ack_x))
            # Y-axis home seq command
            ser.write(('1 i r0 0x800'+ y_home_seq +'\r').encode())
            drive_ack_y = ser.readline()
            drive_ack_y = drive_ack_y.rstrip().decode("utf-8")
            logger.debug("Y-axis Drive ACK: " + str(drive_ack_y))
        except Exception as e:
            logger.error("Serial Error: " + str(e))
        if drive_ack_x  == 'ok' and drive_ack_y == 'ok':
            logger.debug("Drive acked home sequence go cmd.")
            logger.debug("Waiting for homing to complete.")
            while True:
                '''The controller checks bit 13 of the trajectory status
                register to determine if homing is active.
                Set -> Active'''
                ser.write('0 g r0xc9\r'.encode())
                drive_ack = ser.readline()
                drive_ack = drive_ack.rstrip().decode("utf-8")
                logger.debug("Y-axis trajectory stat, drive ack: " + str(drive_ack))
                drive_traj_stat_ack = drive_ack.split()
                if (int(drive_traj_stat_ack[1]) & (1<<13)):
                    pass
                else:
                    '''The controller checks bit 14 of the trajectory status
                    register to determine if the move was aborted.
                    Set -> Aborted'''
                    if (int(drive_traj_stat_ack[1]) & (1<<14)):
                        logger.error("X-Axis homing aborted, exit loop.")
                        # since the move was aborted for some reason, it's best
                        # to abort sequence by setting loop counter to out of range
                        break
                    else:
                        logger.debug("X-axis homing successful..")
                        break
            while True:
                '''The controller checks bit 13 of the trajectory status
                register to determine if homing is active.
                Set -> Active'''
                ser.write('1 g r0xc9\r'.encode())
                drive_ack = ser.readline()
                drive_ack = drive_ack.rstrip().decode("utf-8")
                logger.debug("Y-axis trajectory stat, drive ack: " + str(drive_ack))
                drive_traj_stat_ack = drive_ack.split()
                if (int(drive_traj_stat_ack[1]) & (1<<13)):
                    pass
                else:
                    '''The controller checks bit 14 of the trajectory status
                    register to determine if the move was aborted.
                    Set -> Aborted'''
                    if (int(drive_traj_stat_ack[1]) & (1<<14)):
                        logger.error("Y-axis homing aborted, exit loop.")
                        # since the move was aborted for some reason, it's best
                        # to abort sequence by setting loop counter to out of range
                        break
                    else:
                        logger.debug("Y-axis homing successful..")
                        break
        else:
            logger.error("Drive did not ack, check the drive is on and amp is hardware enabled")
    else:
        logger.info("Serial Port is not opened")


#========================================= Serial Functions here ===========================================
''' The Jetson Xavier-NX communicates with the XYZ-driver over RS232-Serial comms
 The following functions handle the serial comms between the NX and XYZ-driver'''
# This function opens the Jetson's serial port with 115200 baudrate
def SerOpen():
	retry_open = 0
	global ser
	port_path = '/dev/ttyUSB0'
	logger.debug("Port path is: " + port_path)
	while retry_open < 5:
		try:
			ser = serial.Serial(
				port = port_path, baudrate = 9600,
				parity = serial.PARITY_NONE,
				stopbits = serial.STOPBITS_ONE,
				bytesize = serial.EIGHTBITS,
                timeout = 3)
			logger.debug("Opened serial port")
			retry_open = 0
			break
		except Exception as e:
			logger.error("Serial open Error: " + str(e))
			retry_open += 1
			sleep(1)


''' This function closes the serial port. It is important to close the serial port 
when not in use to free up the USB bandwidth'''
def SerClose():
    try:
        ser.close()
        logger.debug("Serial port closed")
    except Exception as e:
        logger.error("Serial close Error: " + str(e))


#========================================== FOTENIX Operation Functions here ===========================================
def CompileJSONData(new_exp, new_rec, plant_number, cat_number, cat_name, cat_plant_number, time_stamp, image_file_name):
    global db_json_dict
    global exp_id
    #command_dict = []
    if new_exp:
        if new_exp:
            exp_id = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        else:
            pass
        db_json_dict = {"commands": [], "experiments": [], "sets": [], "settings": []}
        if os.path.isfile('/mnt/db.json'):
            logger.debug("db.json file exists loading json data.")
            with open('/mnt/db.json') as json_file:
                db_json_dict = json.loads(json_file.read())
        else:
            logger.debug("db.json file doesn't exist creating new one.")
        try:
            experiment_dict = {"name": config.get('EXPERIMENT', 'name'), 
                            "description": config.get('EXPERIMENT', 'description'),
                            "species_type": config.get('EXPERIMENT', 'species_type'),
                            "plant_variety": config.get('EXPERIMENT', 'plant_variety'),
                            "seeding_date": config.get('EXPERIMENT', 'seeding_date'),
                            "number_of_categories": config.getint('EXPERIMENT', 'number_of_categories'),
                            "number_of_plants_per_category": config.getint('EXPERIMENT', 'number_of_plants_per_category'),
                            "id": exp_id,
                            "updated": datetime.now().strftime("%d-%m-%Y-%H-%M-%S")}
            for i in range(config.getint('EXPERIMENT', 'number_of_categories')):
                cat_key = "category_" + str(i)
                experiment_dict[cat_key] = config.get('EXPERIMENT', cat_key)
            settings_list = {"email": config.get('EXPERIMENTSETTINGS', 'email'),
                            "name": config.get('EXPERIMENTSETTINGS', 'name'),
                            "output_folder": "/mnt",
                            "id": 1,
                            "rig_id": "rig5",
                            "capture": "-m -p -c ",
                            "calibrate": "--calibrate"}
            db_json_dict["experiments"].append(experiment_dict)
            db_json_dict["settings"].append(settings_list)
        except Exception as e:
            logger.error("Experiment config file error: " + str(e))
    else:
        set_id = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        sets_dict = {"set_id": rec_number,
                    "experiment_id": exp_id,
                    "plant_number": plant_number,
                    "category_number": cat_number,
                    "category_name": cat_name,
                    "category_plant_number": cat_plant_number,
                    "completed": 1,
                    "skipped": 0,
                    "name": image_file_name,
                    "updated": time_stamp,
                    "id": set_id}
        db_json_dict["sets"].append(sets_dict)


def SaveJSONData(data):
    with open('/mnt/db.json', 'w') as outfile:
        json.dump(data, outfile)
    logger.info("Saved JSON data.")


def StartImageAcquisition():
    global plant_number
    n_of_cat = config.getint('EXPERIMENT', 'number_of_categories')
    pos_dict = {}
    capture_flag = False
    for i in range(n_of_cat):
        pos_dict["c_"+str(i)+"_pos"] = config.get('EXPERIMENT', 'category_'+str(i)+'_positions').split(',')
    for i in range(len(pos_dict)):
        if str(pos_number) in pos_dict["c_"+str(i)+"_pos"]:
            plant_number += 1
            cat_number = i + 1
            cat_name = config.get('EXPERIMENT', 'category_'+str(i))
            cat_plant_number = pos_dict["c_"+str(i)+"_pos"].index(str(pos_number)) + 1
            time_stamp = datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
            exp_name = config.get('EXPERIMENT', 'name')
            image_file_name = '/mnt/exp-' + exp_name.replace(' ', '-') + '-set-1-cat-' + str(cat_number) + '-plant-' + str(cat_plant_number) + '-' + cat_name.replace(' ', '-') + '-' + time_stamp
            CompileJSONData(False, False, plant_number, cat_number, cat_name, cat_plant_number, time_stamp, image_file_name)
            capture_flag = True
            break 
    if capture_flag or pos_number==1:
        foxtrot_cap_cmd = '/home/./foxtrot_cap -m -p -c --laser --dark-image ' + image_file_name
        FoxtrotCapRun(foxtrot_cap_cmd)
    else:
        logger.debug('position: ' + str(pos_number) + ' is empty.')


def FoxtrotCapRun(foxtrot_cap_cmd):
    logger.info("Starting image capture")
    try:
        os.system(foxtrot_cap_cmd)
    except Exception as e:
        logger.error("Foxtrot_cap error: " + str(e))
    logger.info("Image capture complete")

#============================================= MAIN Functions ===============================================
def force_exit_handler():
    logger.info("Forced exiting...")
    sys.exit(0)


def main():
    LoggerInit()
    ReadConfigFile()
    global rec_number
    rec_number = 1
    logger.info("Starting XYZ control program.")
    calib_cam = config.get('EXPERIMENT', 'calibrate_camera')
    if calib_cam == 'Yes':
        FoxtrotCapRun('/home/./foxtrot_cap --calibrate')
    else:
        logger.info("Calibration not set.")
    # This is a new experiment so setting up CompileJSONData with new_exp as True
    # new_rec variable is a don't care in this case 
    CompileJSONData(True, False, 1, 1, ' ', 1, ' ', ' ')
    ExperimentStartDate = config.get('SCHEDULING', 'ExperimentStartDate')
    ExperimentStopDate = config.get('SCHEDULING', 'ExperimentStopDate')
    ExperimentStartTime = config.get('SCHEDULING', 'ExperimentStartTime')
    ExperimentStopTime = config.get('SCHEDULING', 'ExperimentStopTime')
    schedule.every().day.at(ExperimentStartTime).do(RunEnable)
    schedule.every().day.at(ExperimentStopTime).do(RunDisable)
    t_date = datetime.now().strftime("%d/%m/%Y")
    logger.info("Scheduler set waiting till start time.")
    try:
        while True:
            if t_date >= ExperimentStartDate and t_date <= ExperimentStopDate:
               pass
            else:
                break
            schedule.run_pending()
            time.sleep(5)
            t_date = datetime.now().strftime("%d/%m/%Y")
    except KeyboardInterrupt:
        force_exit_handler()


if __name__ == "__main__":
    main()
