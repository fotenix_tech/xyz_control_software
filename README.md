# README #

This repository contains the python codes required to automate the Fallowfiled XYZ system. The python program runs on the Fotenix's Jetson Xavier-NX embedded system. The NX communicates with XYZ drive over serial comms. A dedicated RS232-USB convertor is used to enable this communication.

## DISCLAIMER ##
**Do not change the XYZ parameters unless absolutely certain of the associated behaviour. The code written is for a pre-progammed XYZ sequence with fixed velocity, acceleration, deceleration, etc. if any of these parameters are changed as of (30/08/2020), the author(s) does not take any responsibility for equipment damage, loss of property, injury, or death.**

## DEPENDENCIES ##
* pip3: `sudo apt install python3-pip`
* pyserial: `pip3 install pyserial`
* pathlib: `pip3 install pathlib2`
* schedule: `pip3 install schedule`

## SETUP ##
* On a freshly installed Fotenix environment on the Jetson NX copy the flycap2-conf file, run the file, command:`sudo sh flycap2-conf` and follow the prompt.
* All the data captured using Fotenix camera is stored in the NX's SSD, thus, the SSD must be mounted before running the python program. Also the Fotenix camera requires larger USB memory to function properly make sure to change this parameter. To setup these two parameters to load at reboot. Setup crontab, command: `sudo nano /etc/crontab` and add the following lines at the end of the file.
```
@reboot root mount /dev/nvme0n1p1 /mnt/
@reboot root sh -c 'echo 1024 > /sys/module/usbcore/parameters/usbfs_memory_mb'
```
* Perform a reboot for the above changes to take effect, command: `sudo reboot`
* Copy the latest build of Fotenix `foxtro_cap` executable to `/home/` directory from wherever the executable is available.
* To make sure the above executable runs properly, run the command: `sudo /home/./foxtrot_cap -m -p -c --laser --dark-image /mnt/test` 
* Install all the above dependencies.
* Clone this repository, command: `git clone git@bitbucket.org:fotenix_tech/xyz_control_software.git`
* Create directory `XYZLogs` in `/home/fotenix/`, command: `mkdir XYZLogs`
* Change the experiment parameters in the config file, command: `nano experiment_config.ini`
* Change the verbose levels of the python logger in the `logging.conf` file, command: `nano logging.conf`
* Power on the XYZ-drive and connect the RS232-USB convertor to NX. Check if the XYZ-drive is visible to NX, command: `ls /dev/tty*`. The RS232-USB convertor should show up as `ttyUSB0`.
* Finally run the `main_control.py` to start automation, command: `sudo python3 main_control.py`

## XYZ DRIVE ##
* All the drive related softwares and manuals have been uploaded to the Fotenix Google Drive. The location of these files are `Technology > FALLOWFIELD-XYZ` directory. 
* The CME installer (CME 8.0 Setup.exe) is available in `Technology > FALLOWFIELD-XYZ > CopleySoftware > CME2_Ver_8.0` directory. The software can be used to configure the XYZ-drive. 
* The drive data configuration (configured before 30/08/2020) is available in `Technology > FALLOWFIELD-XYZ > CopleySoftware > PresetDriveData`.
* All the user manuals and datasheets are available in `Technology > FALLOWFIELD-XYZ > Datasheets_Manuals`.

### CONTACTS ###

* Charles Veys
* Bruce Grieve
