import os
import sys
import time
import json
import serial
import signal
import logging
import schedule
import configparser
import logging.config
import random, string
from time import sleep
from pathlib import Path
from threading import Event
from datetime import datetime, timedelta


#===================================== Logger & Config functions here ======================================
''' Read the Experiment parameter from the experiment_config file. Edit this file
before running a new experiment so that the capture file naming and
the associated metadeta is accurate and ready for the analysis software.'''
def ReadConfigFile():
    global config
    print('Reading experiment config file.')
    try:
        currentPath = Path(os.getcwd())
        log_conf_path = currentPath / 'experiment_config.ini'
        config = configparser.ConfigParser()
        config.read(log_conf_path)
    except Exception as e:
        print('Main config file read error: ' + str(e))


def CompileJSONData(new_exp, plant_number, cat_number, cat_name, cat_plant_number):
    global db_json_dict
    global exp_id
    print("Compiling JSON data..")
    #command_dict = []
    if new_exp:
        exp_id = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        db_json_dict = {"commands": [], "experiments": [], "sets": [], "settings": []}
        if os.path.isfile(r'C:\Users\sraj\Desktop\XYZ-Fallowfield\xyz_control_software\db.json'):
            print('db.json file exists loading json data')
            with open(r'C:\Users\sraj\Desktop\XYZ-Fallowfield\xyz_control_software\db.json') as json_file:
                db_json_dict = json.loads(json_file.read())
            print(db_json_dict["experiments"])
        else:
            print("db.json file doesn't exist creating new one.")
        try:
            experiment_dict = {"name": config.get('EXPERIMENT', 'name'), 
                            "description": config.get('EXPERIMENT', 'description'),
                            "species_type": config.get('EXPERIMENT', 'species_type'),
                            "plant_variety": config.get('EXPERIMENT', 'plant_variety'),
                            "seeding_date": config.get('EXPERIMENT', 'seeding_date'),
                            "number_of_categories": config.getint('EXPERIMENT', 'number_of_categories'),
                            "number_of_plants_per_category": config.getint('EXPERIMENT', 'number_of_plants_per_category'),
                            "id": exp_id,
                            "updated": datetime.now().strftime("%d-%m-%Y-%H-%M-%S")}
            for i in range(config.getint('EXPERIMENT', 'number_of_categories')):
                cat_key = "category_" + str(i)
                experiment_dict[cat_key] = config.get('EXPERIMENT', cat_key)
            print(experiment_dict)
            settings_list = {"email": config.get('EXPERIMENTSETTINGS', 'email'),
                            "name": config.get('EXPERIMENTSETTINGS', 'name'),
                            "output_folder": "/mnt",
                            "id": 1,
                            "rig_id": "rig5",
                            "capture": "-m -p -c ",
                            "calibrate": "--calibrate"}
            db_json_dict["experiments"].append(experiment_dict)
            db_json_dict["settings"].append(settings_list)
        except Exception as e:
            print(str(e))
    else:
        time_stamp = datetime.now().strftime("%d-%m-%Y-%H-%M-%S")
        exp_name = config.get('EXPERIMENT', 'name')
        image_file_name = '/mnt/exp-' + exp_name.replace(' ', '-') + '-set-1-cat-' + str(cat_number) + '-plant-' + str(cat_plant_number) + '-' + cat_name.replace(' ', '-') + '-' + time_stamp
        set_id = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
        sets_dict = {"set_id": 1,
                    "experiment_id": exp_id,
                    "plant_number": plant_number,
                    "category_number": cat_number,
                    "category_name": cat_name,
                    "category_plant_number": cat_plant_number,
                    "completed": 1,
                    "skipped": 0,
                    "name": image_file_name,
                    "updated": time_stamp,
                    "id": set_id}
        db_json_dict["sets"].append(sets_dict)

def SaveJSONData(data):
    print("Saving JSON data..")
    with open('db.json', 'w') as outfile:
        json.dump(data, outfile)

#======================================== Scheduling Functions here ========================================
def ExperimentScheduling():
    global stop_experiment
    global stop_for_today
    stop_experiment = False
    stop_for_today = False
    exp_stat = ''
    print("Starting scheduling.")
    ExperimentStartDate = config.get('SCHEDULING', 'ExperimentStartDate')
    ExperimentStopDate = config.get('SCHEDULING', 'ExperimentStopDate')
    ExperimentStartTime = config.get('SCHEDULING', 'ExperimentStartTime')
    ExperimentStopTime = config.get('SCHEDULING', 'ExperimentStopTime')
    t_date = datetime.now().strftime("%d/%m/%Y")
    if t_date >= ExperimentStartDate and t_date <= ExperimentStopDate:
        print("Date today equals Experiment Start Date.")
        t_time = datetime.now().strftime("%H:%M")
        if t_time >= ExperimentStartTime and t_time <= ExperimentStopTime:
            print("Time now equals Experiment Start Time:" + str(t_time))
            # run seq
            exp_stat = 'run_seq'
            return exp_stat
        else :
            print("Time now equals Experiment Stop Time.")
            # stop seq
            exp_stat = 'stop_seq'
            stop_for_today = True
            return schedule.CancelJob
    else:
        print("Date today equals Experiment Stop Date.")
        # stop the experiment
        exp_stat = 'stop_exp'
        stop_experiment = True
        return schedule.CancelJob

def force_exit_handler():
    print('Forced exiting...')
    #sys.exit(0)

def RunEnable():
    global stop_for_today
    stop_for_today = False
    delay_cap = config.getint('SCHEDULING', 'DelayBetweenCaptures')
    schedule.every(delay_cap).minutes.do(RunXSequence)
    #schedule.every(delay_cap).seconds.do(RunXSequence)
    print('Program is activated for today...')

def RunDisable():
    global stop_for_today
    stop_for_today = True
    print('Program is deactivated till tomorrow...')

def RunXSequence():
    if stop_for_today:
        return schedule.CancelJob
    else:
        print('Starting XSequence, time now: ' + datetime.now().strftime("%d/%m/%Y/%H:%M:%S"))

def main():
    ReadConfigFile()
    ExperimentStartDate = config.get('SCHEDULING', 'ExperimentStartDate')
    ExperimentStopDate = config.get('SCHEDULING', 'ExperimentStopDate')
    ExperimentStartTime = config.get('SCHEDULING', 'ExperimentStartTime')
    ExperimentStopTime = config.get('SCHEDULING', 'ExperimentStopTime')
    schedule.every().day.at(ExperimentStartTime).do(RunEnable)
    schedule.every().day.at(ExperimentStopTime).do(RunDisable)
    t_date = datetime.now().strftime("%d/%m/%Y")
    try:
        while True:
            if t_date >= ExperimentStartDate and t_date <= ExperimentStopDate:
               pass
            else:
                break
            schedule.run_pending()
            time.sleep(5)
            t_date = datetime.now().strftime("%d/%m/%Y")
    except KeyboardInterrupt:
        force_exit_handler()
    print('Program complete.')

if __name__ == "__main__":
    main()